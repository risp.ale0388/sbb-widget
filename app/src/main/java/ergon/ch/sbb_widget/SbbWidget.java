package ergon.ch.sbb_widget;

import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.location.Location;
import android.widget.RemoteViews;

/**
 * Implementation of App Widget functionality.
 */
public class SbbWidget extends AppWidgetProvider {


    void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        WidgetService widgetService = new WidgetService(context);
        double latitude = widgetService.getLatitude();
        double longitude = widgetService.getLongitude();
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.sbb_widget);
        views.setTextViewText(R.id.appwidget_text,  String.format("%.3f", latitude) + ", " + String.format("%.3f", longitude) );
        appWidgetManager.updateAppWidget(appWidgetId, views);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}